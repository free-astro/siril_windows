This is the replication of Windows build process developped by RdH for [inkscape](https://gitlab.com/inkscape/deps/windows) and adjusted for Siril.

# Windows build-time dependencies

This repository collects the [msys2](https://www.msys2.org)-based dependencies to build Siril on Windows. The heavy lifting is done mostly by [`siril-deps.sh`](https://gitlab.com/free-astro/siril/-/blob/master/build/windows/native-gitlab-ci/siril-deps.sh) combined with a custom-made minimal build of opencv. The goal is to have a self-contained msys2 installation folder (here: `C:/sirilN`, with `N` being the `CI_PIPELINE_IID`) so we can archive it and use it in CI.

Releases are created manually by tagging a commit using the pattern `rN`, with `N` being the corresponding `CI_PIPELINE_IID` from above. Siril's CI can then be updated to use a newer release.

While this approach creates an additional maintenance burden as updates to `siril-deps.sh` in Siril's repository no longer affect CI immediately, we gain the ability to build Siril against a fixed set of dependencies instead of a moving target (msys2 follows the rolling release model and does not provide an archive of old versions), and reproducible builds are of much higher value to us.

## license

[GPL-2.0-or-later](LICENSE)
