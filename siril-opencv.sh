# Build minimal opencv
cd opencv_minbuild
dos2unix PKGBUILD
updpkgsums
makepkg-mingw --cleanbuild --syncdeps --force --noconfirm --install
cd ..
rm -Rf opencv_minbuild